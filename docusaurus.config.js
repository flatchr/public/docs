// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer').themes.github;
const darkCodeTheme = require('prism-react-renderer').themes.dracula;


/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Flatchr Docs',
  tagline: 'flatchr.io | Logiciel de recrutement tentaculaire',
  url: 'https://www.flatchr.io/',
  baseUrl: '/',
  onBrokenLinks: 'log',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/logo.svg',
  organizationName: 'flatchr.io', // Usually your GitHub org/user name.
  projectName: 'docs', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/flatchr/public/docs/-/edit/main/',

        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/flatchr/public/docs/-/edit/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  // plugins: [
  //   [
  //     require.resolve("@cmfcmf/docusaurus-search-local"),
  //     {
  //       language: "fr",
  //     }
  //   ],
  // ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Flatchr Docs',
        logo: {
          alt: 'Logo Fl',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'getting_started',
            position: 'left',
            label: 'API',
          },
          {to: '/site-carriere', label: 'Site Carrière', position: 'left'},
          {to: '/sso', label: 'SSO', position: 'left'},
          {to: '/webhooks', label: 'Webhooks', position: 'left'},
          {href: 'https://www.postman.com/flatchr/workspace/flatchr-public-api/overview', label: 'Run in Postman', position: 'right',className:'postman_navbar__link'},
        ],
      },
      footer: {
        links: [
          {
            title: 'Documentation',
            items: [
              {
                label: 'API',
                to: '/docs/getting_started',
              },
              {to: '/site-carriere', label: 'Site Carrière'},
              {to: '/sso', label: 'SSO'},
              {to: '/webhooks', label: 'Webhooks'},
              {href: 'https://www.postman.com/flatchr/workspace/flatchr-public-api/overview', label: 'Run in Postman',className:'postman_footer__link-item'},
            ],
          },
          {
            title: 'Communauté',
            items: [
              {
                label: 'Linkedin',
                href: 'https://www.linkedin.com/company/flatchr/',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/Flatchr',
              },
              {
                label: 'Instagram',
                href: 'https://www.instagram.com/flatchr.io/',
              },

            ],
          },
          {
            title: 'En savoir plus',
            items: [

              {
                label: 'Flatchr.io',
                href: 'https://www.flatchr.io/',
              },
              {
                label: 'Blog',
                href: 'https://blog.flatchr.io/',
              },
              
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Flatchr International `,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme
      },
      colorMode: {
        defaultMode: 'light',
        disableSwitch: false,
        respectPrefersColorScheme: false
      },
    }),
};

module.exports = config;