### Flatchr documentation

This documentation is build with [Docusaurus](https://docusaurus.io/).

To work with:

- Clone this repo
- Install dependencies `npm install`
- Run the project with `npm run start`

---

## GitLab CI/CD

This project's static Pages are built by [GitLab CI/CD](https://about.gitlab.com/product/continuous-integration/),
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
image: node:19-alpine3.15

stages:
  - test
  - deploy

test:
  stage: test
  script:
  - yarn install
  - yarn build
  except:
    - main

pages:
  stage: deploy
  script:
  - yarn install
  - yarn build
  - mv ./build ./public
  artifacts:
    paths:
    - public
  only:
    - pages
```