---
sidebar_position: 4
description: Foire Aux Questions
---

# Foire Aux Questions

Vous trouverez dans cette page les réponses aux cas d'usage les plus fréquents, ainsi que des informations permettant la mise en place de cas plus spécifiques.

### Je souhaite récupérer la lettre de motivation au format PDF de mes candidats. Comment puis-je faire ?

L'API de Flatchr ne permet pas de transmettre un fichier en plus du CV (ex: lettre de motivation) lors de la création d'un candidat.
Cependant, nous pouvons vous proposer 2 solutions de contournement.

**Solution 1 : Utiliser le système de questions pour récupérer un fichier de lettre de motivation.**

Pour cela, il faut réaliser les actions suivantes :
- Créer une question nécessitant la saisie d'une lettre de motivation
- Récupérer l'id de la question créée. Vous le trouverez dans l'objet vacancy
- Ajouter l'id de cette question dans le payload de la requête de création de candidat :

```jsx
var formdata = new FormData();

formdata.append("firstname", "Teresa");
formdata.append("lastname", "Breitenberg");
formdata.append("answers[QUESTION_ID]", "https://url-du-fichier/fichier.pdf");
formdata.append("vacancy", "SLUG_VACANCY");

var requestOptions = {
  method: 'POST',
  body: formdata,
  redirect: 'follow'
};

fetch("https://careers.flatchr.io/vacancy/candidate/custom", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

En utilisant cette solution, la lettre de motivation sera affichée de cette manière sur la fiche candidat :

![exemple clé d'entrpris](/img/screenshot_faq_lettre_motivation_01.png)


**Solution 2 : Utiliser le champ "comment" pour récupérer une lettre de motivation au format texte.**

Pour cela, il suffit juste d'ajouter le champ "comment" dans le payload de création de candidat.

![exemple clé d'entrpris](/img/screenshot_faq_lettre_motivation_02.png)


```jsx
var formdata = new FormData();

formdata.append("firstname", "Teresa");
formdata.append("lastname", "Breitenberg");
formdata.append("vacancy", "SLUG_VACANCY");
formdata.append("comment", "Lettre de motivation du candidat");

var requestOptions = {
  method: 'POST',
  body: formdata,
  redirect: 'follow'
};

fetch("https://careers.flatchr.io/vacancy/candidate/custom", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
```

En utilisant cette solution, la lettre de motivation sera affichée de cette manière sur la fiche candidat :

![exemple clé d'entrpris](/img/screenshot_faq_lettre_motivation_03.png)
