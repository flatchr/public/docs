# experience

Le référentiel de l'expérience requise pour une annonce : 

| Valeur | Description                   |
| ------ | ----------------------------- |
| 1      | Aucune ( < 1 an )             |
| 2      | Moyenne ( entre 1 et 3 ans )  |
| 4      | Beaucoup ( entre 3 et 5 ans ) |
| 6      | Cador ( > 6 ans )             |
