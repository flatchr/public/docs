---
sidebar_position: 6
---


# Récupérer les Cvs de vos candidats

Vous pouvez récupérer le(s) CV(s) de vos candidats grâce à l'API.

## Requête

Cette méthode permet de récupérer le(s) CV(s) de vos candidats : 

```
GET /company/{companyID}/files/expire?key={Clé_CV}&extension={Extension_CV}&token={Clé_API}
```

:::caution Important

La Clé API est valable 365 jours et expire automatiquement. Nous déclenchons une alerte 15 jours avant leur expiration. Vous pouvez créé votre clé API et retrouver ces informations à cette adresse : https://app.flatchr.io/board/settings/advanced/api

La "Clé CV" (key) et l'"Extension CV" (extension) correspondent à l'identifiant récupéré dans le webhook au niveau de CV

```json
"cv": {
  "fileable_id": 3935678,
  "fileable_type": "candidates",
  "id": 3929083,
  "url": "flatchr.s3.eu-west-1.amazonaws.com/candidates/2024/01/c18f5238-a774-4609-8d59-f022a8f781db/ivern-elsa.pdf",
  "key": "candidates/2024/01/c18f5238-a774-4609-8d59-f022a8f781db/ivern-elsa",
  "type": "CV",
  "created_at": "2023-12-12T15:58:43.942Z",
  "updated_at": "2024-01-09T12:23:39.544Z",
  "extension": "pdf"
}
```

:::

### Paramètres
| Paramètre | In   | Type   | Obligatoire        | Description                                                                     | Exemple                                                                |
| --------- | ---- | ------ | ------------------ | ------------------------------------------------------------------------------- | ---------------------------------------------------------------------- |
| companyId | path | string | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise) | Wy3EOp2NP3p1KMq8                                                       |
| key       | path | string | <center>✔️</center> | Clé unique de votre CV disponible via le webhook                                | candidates/2024/01/c18f5238-a774-4609-8d59-f022a8f781db/**ivern**-elsa |
| extension | path | string | <center>✔️</center> | Extension du document                                                           | pdf                                                                    |
| token     | path | string | <center>✔️</center> | Token permettant d'accéder à l'API                                              | xxxxxxxxxx                                                             |

### Exemple de requête

```shell
curl -X GET https://api.flatchr.io/company/2zNDRr9BMnYqXyEK/files/expire?key=candidates/2024/01/c18f5238-a774-4609-8d59-f022a8f781db/ivern-elsa&extension=pdf&token=XXXXXXX
```