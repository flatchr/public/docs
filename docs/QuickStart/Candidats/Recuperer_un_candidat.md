---
sidebar_position: 1
---


# Récupérer les candidats

Vous pouvez récupérer les candidats grâce à l'API.


## Requête

Cette méthode permet de rechercher un (ou plusieurs) candidats : 

```jsx
POST /company/{companyID}/search/applicants
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)


### Paramètres
| Paramètre    | In      | Type    | Obligatoire        | Description                                                                                 | Exemple          |
| ------------ | ------- | ------- | ------------------ | ----------------------------------------------------------------------------------------    | ---------------- |
| companyId    | path    | string  | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise)             | Wy3EOp2NP3p1KMq8 |
| firstname    | payload | string  |                    | `firstname` du [candidat](../../Schemas/candidate.md)                                       | John             |
| lastname     | payload | string  |                    | `lastname` du [candidat](../../Schemas/candidate.md)                                        | Doe              |
| email        | payload | string  |                    | `email` du [candidat](../../Schemas/candidate.md)                                           | john.doe@mail.fr |
| hired        | payload | boolean |                    | Le candidat est-il recruté ou non ?                                                         | true             |
| column       | payload | string  |                    | `title` de la [colonne](../../Schemas/vacancy.md) dans laquelle se trouve le candidat       | Entretien RH     |
| start        | payload | string  |                    | Date au format `MM/JJ/AA` pour rechercher les candidats créés à partir d'une date donnée    | 11/15/22         |
| end          | payload | string  |                    | Date au format `MM/JJ/AA` pour rechercher les candidats créés avant une date donnée"        | 12/25/22         |
| start_update | payload | string  |                    | Date au format `MM/JJ/AA` pour rechercher les candidats modifiés à partir d'une date donnée | 11/15/22         |
| end_update   | payload | string  |                    | Date au format `MM/JJ/AA` pour rechercher les candidats modifiés avant une date donnée      | 12/25/22         |
| company      | payload | string  |                    | Permet de rechercher sur plusieurs entreprises pour les multicomptes                        |                  |
| vacancy      | payload | string  |                    | `id` de l'[offre](../../Schemas/vacancy.md) dans laquelle se trouve le candidat             | k5aMxpQoK7nGZ2Oz |
| phone        | payload | string  |                    | `phone` du [candidat](../../Schemas/candidate.md)                                           | 06 06 06 06 06   |

### Exemples de requête

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


<Tabs>
<TabItem value="name" label="Nom" default>

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"lastname":"{nom}"}'
```

</TabItem>
<TabItem value="mail" label="Email" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"email":"{mail@mail.com}"}'
```

</TabItem>
<TabItem value="hired" label="Recruté" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"hired":"true"}'
```

</TabItem>
<TabItem value="archived_candidates" label="Archivés" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"status": 0}'
```

</TabItem>

<TabItem value="since" label="Depuis le 25/12/2021" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"start":"12/25/21"}'
```

</TabItem>
<TabItem value="between" label="Entre le 01/01/2022 et le 10/01/2022" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"start":"01/01/22", "end":"01/10/22"}'
```

</TabItem>
<TabItem value="company" label="Multicomptes" >

```jsx title="Requête cURL"
curl -X POST https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/search/applicants
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"hired":"true","company":"{company}"}'
```

</TabItem>

</Tabs>

## Réponse
| Name        | Type      | Description                                            |
| ----------- | --------- | ------------------------------------------------------ |
| applicant   | string    | Id du candidat                                         |
| vacancy     | string    | Nom de l'offre sur laquelle il a postulé               |
| column      | string    | Colonne dans laquelle est le candidat                  |
| vacancy_id  | integer   | Identifiant de l'offre                                 |
| external_id | string    | Id externe                                             |
| status      | integer   | Statut du candidat (1: Actif, 2: Archivé)              |
| score       | integer   | Score du candidat                                      |
| hired       | boolean   | Le candidat est-il recruté ?                           |
| firstname   | string    | Prénom                                                 |
| lastname    | string    | Nom                                                    |
| email       | string    | Mail                                                   |
| phone       | string    | Téléphone                                              |
| created_at  | timestamp | Date de création du candidat (JJ/MM/AA)                |
| updated_at  | timestamp | Date de modification du candidat (JJ/MM/AA)            |
| source      | string    | Source de la candidature                               |
| reason      | string    | Motif de recrutement ou de refus                       |
| import      | boolean   | Le candidat a t'il été importé ?                       |
| creator     | string    | Nom Prénom du membre qui a coopté le candidat          |

### Exemple de réponse

<Tabs>
<TabItem value="standard" label="Réponse type" default>

```json
[{
    "applicant": "BEjyY9lke8nZl7o2",
    "vacancy": " Commercial(e) - Paris - CDI ",
    "column": "Entretien RH",
    "vacancy_id": 2411,
    "external_id": "",
    "status": 1,
    "score": 90,
    "hired": false,
    "firstname": "michelle",
    "lastname": "dekerre",
    "email": "michelle.d@mail.com",
    "phone": "",
    "created_at": "15/11/24",
    "source": "Site carrière",
    "import": true
}]
```

</TabItem>

<TabItem value="archived" label="Archivé">

```json
[{
    "applicant": "2o7lZn8ekl9YyjEB",
    "vacancy": " Commercial(e) - Paris - CDI ",
    "column": "Entretien RH",
    "vacancy_id": 2411,
    "external_id": "",
    "status": 0,
    "score": 10,
    "hired": false,
    "firstname": "Joe",
    "lastname": "Seant",
    "email": "joe.seant@mail.com",
    "phone": "",
    "created_at": "15/11/24",
    "source": "Site carrière",
    "reason": "Manque d'expérience pour le poste",
    "import": true
}]
```

</TabItem>

</Tabs>