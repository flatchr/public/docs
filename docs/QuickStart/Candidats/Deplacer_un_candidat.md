---
sidebar_position: 3
---

# Déplacer un candidat

Vous pouvez déplacer un candidat grâce à l'API.

## Requête

Cette méthode permet de déplacer des candidats : 

```jsx
PUT /company/{companyID}/vacancy/{vacancyID}/applicant/{applicantID}
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)

### Paramètres
| Paramètre   | In      | Type    | Obligatoire        | Description                                                                        | Exemple          |
| ----------- | ------- | ------- | ------------------ | ---------------------------------------------------------------------------------- | ---------------- |
| companyID   | path    | string  | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started.md#identifiant-de-lentreprise) | 0GjqBwprWAdArDl4 |
| vacancyID   | path    | string  | <center>✔️</center> | `id` de l'[offre](/docs/Schemas/vacancy.md)                                        | Vq5r6pYxam9AmMvP |
| applicantID | path    | string  | <center>✔️</center> | `id` du [candidat](/docs/Schemas/candidate.md)                                     | V41QG9eja5pK6Xev |
| column_id   | payload | integer | <center>✔️</center> | `id` de la [colonne](/docs/Schemas/vacancy.md)                                     | 13133            |

### Exemple de requête

```jsx title="Requête cURL"
curl -X PUT 'https://api.flatchr.io/company/0GjqBwprWAdArDl4/vacancy/Vq5r6pYxam9AmMvP/applicant/V41QG9eja5pK6Xev'
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"column_id":13133}'
```


## Réponse
| Name        | Type                                    | Description                                             |
| ----------- | --------------------------------------- | ------------------------------------------------------- |
| id          | string                                  | Identifiant du candidat                                 |
| vacancy_id  | string                                  | Identifiant de l'offre                                  |
| column_id   | integer                                 | Identifiant de la colonne dans laquelle est le candidat |
| created_at  | timestamp                               | Date de création du candidat                            |
| updated_at  | timestamp                               | Date de mise à jour du candidat                         |
| status      | integer                                 | Statut du candidat                                      |
| view        | boolean                                 | La fiche de ce candidat a-t-elle été consultée ?        |
| anonym      | boolean                                 | Le candidat est-il anonymisé ?                          |
| external_id | string                                  | Identifiant externe du candidat                         |
| candidate   | [candidate](/docs/Schemas/candidate.md) | Les informations du candidat                            |
| column      | string                                  | Colonne vers laquelle est déplacé le candidat           |
| vacancy     | string                                  | Nom de l'offre sur laquelle a postulé le candidat       |
| foreigner   | boolean                                 |                                                         |
| applies     | [[applies](/docs/Schemas/applies.md)]   |                                                         |

### Exemple de réponse

```jsx
{
    "id": "V41QG9aja5pK6Xev",
    "vacancy_id": "Vq1r6pYxam9AmMvP",
    "column_id": 1313,
    "score": 0,
    "comment": "",
    "created_at": 1643904014,
    "updated_at": 1645015716,
    "status": 1,
    "view": true,
    "anonym": false,
    "external_id": null,
    "candidate": {
        "id": "LrENkKc8mmn3xYBM",
        "email": "xxxxxxxx@gmail.com",
        "firstname": "XXXX",
        "lastname": "XXXX",
        "urls": {},
        "created_at": "2022-02-03T16:00:14.589Z",
        "phone": "+33 0 00 00 00 00",
        "consent": true,
        "cv": "http://flatchr-staging.s3.eu-west-1.amazonaws.com/candidates/2022/02/27fb122b-71cf-4acc-a7a4-32c6807a03b9/xxxxx-xxxxx.pdf",
        "aws": {
            "key": "candidates/2019/02/67fb112b-71cf-4acc-a7a4-32c6807a03b9/alexandre-andrieux",
            "extension": "pdf",
            "signature": "s3.eu-west-1.amazonaws.com/flatchr-demo/candidates%2F2022%2F02%2F67fb112b-71cf-4acc-a7a4-32c6807a03b9%2Falexandre-andrieux.pdf?X-Amz-Expires=86400&X-Amz-Date=20220216T124836Z&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJHRG2QM2E256YXMA%2F20220216%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-SignedHeaders=host&X-Amz-Signature=d8c382898a0936429fc3b56247fe6e37f2280df71a5a3a9864a127375d6fcab8"
        }
    },
    "column": {},
    "vacancy": {},
    "foreigner": false,
    "applies": [
        {
            "id": "Vq5r8pY1YgpAmMvP",
            "created_at": "2022-02-03T16:00:14.630Z",
            "vacancy_id": "Vq5rnpYxam9AmMvP",
            "offerer_id": "ZmbMGYdOZ1d3JPNx",
            "import": false
        }
    ]
}
```
