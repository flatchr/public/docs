---
sidebar_position: 2
---
# Créer un candidat

Vous pouvez créer un candidat grâce à l'API.

:::caution Important

L'URL racine est ici https://careers.flatchr.io

:::


## Requête

Cette méthode permet de créer un candidat : 

<Tabs>

  <TabItem value="api" label="Par une requête sur l'API" default>

  ```jsx
    POST vacancy/candidate/json
  ```
  [![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-4f87a56b-e6a0-4c9a-86cc-9c9de3aded2f?action=collection%2Ffork&collection-url=entityId%3D18861404-4f87a56b-e6a0-4c9a-86cc-9c9de3aded2f%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422#?env%5BFlatchr%5D=W3sia2V5IjoiYXBpX3Rva2VuIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlLCJ0eXBlIjoic2VjcmV0In0seyJrZXkiOiJzbHVnIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlLCJ0eXBlIjoiZGVmYXVsdCJ9LHsia2V5IjoiY29tcGFueSIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZSwidHlwZSI6ImRlZmF1bHQifV0=)

  </TabItem>
  <TabItem value="web" label="Via un système web">

  ```jsx
    POST vacancy/candidate/custom
  ```
  [![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-4f87a56b-e6a0-4c9a-86cc-9c9de3aded2f?action=collection%2Ffork&collection-url=entityId%3D18861404-4f87a56b-e6a0-4c9a-86cc-9c9de3aded2f%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422#?env%5BFlatchr%5D=W3sia2V5IjoiYXBpX3Rva2VuIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlLCJ0eXBlIjoic2VjcmV0In0seyJrZXkiOiJzbHVnIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlLCJ0eXBlIjoiZGVmYXVsdCJ9LHsia2V5IjoiY29tcGFueSIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZSwidHlwZSI6ImRlZmF1bHQifV0=)

  </TabItem>
</Tabs>


### Paramètres
| Paramètre               | In      | Type                             | Obligatoire        | Description                                                  | Exemple                                                                     |
| ----------------------- | ------- | -------------------------------- | ------------------ | ------------------------------------------------------------ | --------------------------------------------------------------------------- |
| vacancy                 | payload | string                           | <center>✔️</center> | `slug` de l'[offre](/docs/Schemas/vacancy)                   | vyja3k5rrnlnqwe-technicien-h-f                                              |
| firstname               | payload | string                           | <center>✔️</center> | `firstname` du [candidat](/docs/Schemas/candidate)           | Johnny                                                                      |
| lastname                | payload | string                           | <center>✔️</center> | `lastname` du [candidat](/docs/Schemas/candidate)            | Doe                                                                         |
| token                   | payload | string                           | <center>✔️</center> | [Token](/docs/getting_started#token)                         |                                                                             |
| email                   | payload | string                           |                    | `email` du [candidat](/docs/Schemas/candidate)               | john.doe@mail.fr                                                            |
| phone                   | payload | integer                          |                    | `phone` du [candidat](/docs/Schemas/candidate)               | +33123456789                                                                |
| type                    | payload | string                           | <center>✔️</center> | Définit le type de CV (`link` ou `document`)                 | link                                                                        |
| resume                  | payload | base64  / url                    | <center>✔️</center> | Url si `"type": "link"`, <br/> Objet si `"type": "document"` | storage.s3.eu-west-1.amazonaws.com/CV/5b400fab-679811f70b0e/CV_John_Doe.pdf |
| comment                 | payload | string                           |                    | Lettre de motivation du candidat                             |                                                                             |
| offerer_id              | payload | integer                          |                    | [Offerer id](/docs/getting_started#offerer-id)               | 75                                                                          |
| urls                    | payload | object                           |                    | `urls` du [candidat](/docs/Schemas/candidate)                |                                                                             |
| legalNewsletterPartners | payload | boolean                          |                    | Opt-in newsletter (autorisation d'envoie d'une Newsletter)   | false                                                                       |
| similarities            | payload | boolean                          |                    | Retourne offres similaires                                   | false                                                                       |
| response_text           | payload | string                           |                    | Modifier la réponse réglée                                   |                                                                             |
| answers                 | payload | [[answer](/docs/Schemas/answer)] |                    | Réponses aux questions/tags du formulaire                    |                                                                             |
| user_id                 | payload | string                           |                    | Clé utilisateur (Paramètres ➝ Avancés ➝ API)                 | lVq5r6pYLD9AmMvP                                                            |

<br/>

:::tip Astuce

Vous pouvez tester vos requêtes de création de candidats grâce à la méthode suivante : 

```jsx
    POST /vacancy/candidate/test
```

:::


### Exemple de requête

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>

  <TabItem value="pdf" label="Avec un fichier PDF" default>

  ```jsx title="Créer un candidat avec le lien d'un CV"
    curl -X POST https://careers.flatchr.io/vacancy/candidate/json
        -H "Authorization: Bearer {token}"
        -H 'Content-Type: application/json'
        -d '{
            "vacancy": "vyja3k5rrnlnqwe-technicien-h-f",
            "firstname": "Johnny",
            "lastname": "Doe",
            "token": "{token}",
            "type": "document",
            "resume": {
              "data": "{fichier encodé en base 64}",
              "fileName": "CV_John_Doe",
              "contentType": "application/pdf"
            }
          }'
  ```

  </TabItem>
  <TabItem value="link" label="Avec un lien vers le CV">

  ```jsx title="Créer un candidat avec le lien d'un CV"
    curl -X POST https://careers.flatchr.io/vacancy/candidate/json
        -H "Authorization: Bearer {token}"
        -H 'Content-Type: application/json'
        -d '{
                "vacancy": "vyja3k5rnlnqwe-technicien-h-f",
                "firstname": "John",
                "lastname": "Doe",
                "token": "{token}",
                "type": "link",
                "resume": "storage.s3.eu-west-1.amazonaws.com/CV/5b400fab-679811f70b0e/CV_John_Doe.pdf"
              }' 

  ```
  </TabItem>
</Tabs>

D'autres exemples sont disponibles sur [Gitlab](https://gitlab.com/flatchr/example) : 

<center>
  <a class="button button--outline button--secondary" href="https://gitlab.com/flatchr/example/-/blob/master/post_node.js" style={{height: 5 + 'em', width:10 + "em"}}>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/128px-Node.js_logo.svg.png"/>
  </a>
  <a class="button button--outline button--secondary" href="https://gitlab.com/flatchr/example/-/blob/master/post_php.php"style={{height: 5 + 'em', width:10 + "em"}}>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/128px-PHP-logo.svg.png"/>
  </a>
</center>


## Réponse
| Name     | Type        | Description             |
| -------- | ----------- | ----------------------- |
| status   | HTTP Status |                         |
| response | string      | Ex: "applicant created" |
