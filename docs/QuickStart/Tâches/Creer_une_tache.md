---
sidebar_position: 2
---

# Créer une tâche 

Vous pouvez créer une tâche grâce à l'API.

## Requête 

Cette méthode permet de créer une tâche :

```jsx
POST /company/{companyID}/task
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)

### Paramètres
|Paramètre|In|Type|Obligatoire|Description|Exemple|
|---|---|---|---|---|---|
companyID | path | string | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise) |Wy3EOp2NP3p1KMq8|
description | payload | string | <center>✔️</center> |`description` de la [tâche](/docs/Schemas/task) |Traiter ses mails|
date | payload | timestamp | <center>✔️</center> |`date` de la [tâche](/docs/Schemas/task) |2022-02-12T18:55:00.000Z|
email | payload | boolean |  | Est-ce que les notifications seront envoyées par email ? |true|
value | payload | [string] | <center>✔️</center> |`value` de la [tâche](/docs/Schemas/task) |["member=JBMQj09ZW7pxoRLO"]|
users | payload | [string] | <center>✔️</center> |`users` de la [tâche](/docs/Schemas/task) |["JBMQj09ZW7pxoRLO"]|
memberId | payload | string | <center>✔️</center> |`memberId` de la [tâche](/docs/Schemas/task) |JBMQj09ZW7pxoRLO|

### Exemple de requête

```jsx title="Requête cURL"
curl -X POST 'https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/task'
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
    -d '{"description":"Traiter ses mails","date":"2022-02-12T18:55:00.000Z","email":true,"value":["member=JBMQj09ZW7pxoRLO"],"users":["JBMQj09ZW7pxoRLO"],"memberId":"JBMQj09ZW7pxoRLO"}'
```

## Réponse
|Name|Type|Description|
|---|---|---|
id | string | Identifiant de la tâche |
description | string | Description de la tâche |
value | [[string]] | Sur quel élément(s) porte(ent) la tâche : member pour un utilisateur et applicant pour un candidat |
type | string | Type (toujours égal à "task") |
date | timestamp | Date de la tâche |
done | boolean | Est-ce que la tâche a été effectuée ? |

### Exemple de réponse

```jsx
{
    "id": "X2kea8pXr0nN1o0B",
    "description": "Traiter ses mails curl",
    "value": ["member=JBMQj09ZW7pxoRLO"],
    "type": "task",
    "date": "2022-02-12T18:55:00.000Z",
    "done": false
}
```
