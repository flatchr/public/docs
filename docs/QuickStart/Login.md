---
sidebar_position: 1
---
# Login

Vous pouvez vous connecter à l'API.

## Requête

Pour certaines requêtes il est nécessaire de les effectuer avec un token utilisateur que vous pouvez récupérer de la manière suivante :

```jsx
POST /user/login
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)

### Paramètres

| Paramètre | In    | Type    | Obligatoire        | Description                                    | Exemple         |
| --------- | ----- | ------- | ------------------ | ---------------------------------------------- | --------------- |
| email     | query | boolean | <center>✔️</center> | `email` de l'[utilisateur](/docs/Schemas/user) | test@flatchr.io |
| password  | query | string  | <center>✔️</center> | Mot de passe de l'utilisateur                  | #Poulpe45       |

### Exemple de requête

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
<TabItem value="linux" label="Utilisateurs sous OSX - GNU/Linux">

```sh title="Requête cURL"
      curl -X POST 'https://api.flatchr.io/user/login'
      -H 'Content-Type: application/json'
      -d '{"email":"test@flatchr.io", "password":"#Poulpe45"}'
```

</TabItem>
<TabItem value="windows" label="Utilisateurs sous Windows">

```sh title="Requête cURL"
      curl -X POST "https://api.flatchr.io/user/login"
      -H "Content-Type: application/json"
      -d "{\"email\":\"test@flatchr.io\",\"password\":\"#Poulpe45\"}"
```

  </TabItem>
</Tabs>


## Réponse

| Name       | Type    | Description             |
| ---------- | ------- | ----------------------- |
| statusCode | integer | le statut de la requête |
| token      | string  | le token utilisateur    |

### Exemple de réponse

```json
{
    "statusCode": 200,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWlmQGZsYXRjaHIuaW8iLCKpYXQiOjE2NDQ8NDkxNjQsImV4cCI6MTY0NTM4MTE2NH0.yfbr7HQjx90eZdXr9gEQ7FfqQoV4Lm53oZDhC0DMmMU"
}
```
