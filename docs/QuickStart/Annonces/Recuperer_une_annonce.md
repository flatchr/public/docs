---
sidebar_position: 2
---
# Récupérer une annonce

:::caution Important

L'URL racine est ici https://api.flatchr.io

:::

## Requête

Il est possible de récupérer une annonce en utilisant la requête suivante : 

```jsx
GET /vacancy/{vacancyId}
```

### Paramètres
| Paramètre | In    | Type   | Obligatoire        | Description               | Exemple          |
| --------- | ----- | ------ | ------------------ | ------------------------- | ---------------- |
| vacancyId | path  | string | <center>✔️</center> | Identifiant de l'annonce  | Wy3EOp2NP3p1KMq8 |
| fields    | query | string |                    | Informations optionnelles | address          |

### Exemple de requête

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
<TabItem value="no_fields" label="Sans fields" default>

```jsx title="Requête cURL"
curl -X GET https://api.flatchr.io/vacancy/Wy3EOp2NP3p1KMq8
```

</TabItem>

<TabItem value="fields" label="Avec fields">

```jsx title="Requête cURL"
curl -X GET https://api.flatchr.io/vacancy/Wy3EOp2NP3p1KMq8?fields=address
```

</TabItem>
</Tabs>


## Réponse
| Name               | Type                                       | Description                                                      |
| ------------------ | ------------------------------------------ | ---------------------------------------------------------------- |
| id                 | string                                     | Identifiant unique de l'offre d'emploi                           |
| vacancy_id         | integer                                    | ID de l'offre                                                    |
| slug               | string                                     | URL slug de l'offre d'emploi                                     |
| reference          | string                                     | Référence de l'offre d'emploi                                    |
| title              | string                                     | Titre de l'offre d'emploi                                        |
| description        | string                                     | Description de l'offre d'emploi                                  |
| experience         | integer                                    | Expérience requise                                               |
| mission            | string                                     | Description des missions du poste                                |
| profile            | string                                     | Profil requis pour le poste                                      |
| salary             | string                                     | Salaire proposé                                                  |
| status             | integer                                    | État de l'offre                                                  |
| language           | string                                     | Langue de l'offre d'emploi                                       |
| contract_type_id   | integer                                    | ID du type de contrat                                            |
| education_level_id | integer                                    | ID du niveau d'éducation requis                                  |
| activity_id        | integer                                    | ID de l'activité                                                 |
| channel_id         | integer                                    | ID du canal de diffusion de l'offre                              |
| metier_id          | integer                                    | ID du métier                                                     |
| company_id         | integer                                    | ID de l'entreprise                                               |
| mensuality         | enum (y,m,d,h)                             | Fréquence de paiement du salaire (year, month, day, hour)        |
| apply_url          | Null                                       | URL pour postuler à l'offre                                      |
| currency           | string                                     | Devise du salaire                                                |
| created_at         | string                                     | Date de création de l'offre                                      |
| updated_at         | string                                     | Date de la dernière mise à jour de l'offre                       |
| start_date         | Null                                       | Date de début du contrat                                         |
| end_date           | Null                                       | Date de fin du contrat                                           |
| driver_license     | boolean                                    | Si un permis de conduire est requis                              |
| remote             | enum                                       | Poste en télétravail ("notime", "parttime", "fulltime")          |
| handicap           | boolean                                    | Si le poste est adapté aux personnes handicapées                 |
| partial            | boolean                                    | Si le poste est à temps partiel                                  |
| meta_title         | Null                                       | Meta-titre pour le SEO                                           |
| meta_description   | Null                                       | Meta-description pour le SEO                                     |
| meta_tags          | Null                                       | Meta-tags pour le SEO                                            |
| options            | Object [options](/docs/Schema/options)     | Options de l'offre pour sa visualisation sur les sites carrières |
| kanban             | boolean                                    | Si l'offre utilise Kanban                                        |
| slug_mail          | string                                     | Slug pour l'email                                                |
| address            | Object [address](/docs/Schemas/address.md) | Ensemble des informations de l'adresse de l'offre                |


### Exemple de réponse

<details>
<summary> Voir l'exemple </summary>

```json
{
    "id": "Wy3EOp2NP3p1KMq8",
    "vacancy_id": 1950,
    "slug": "v41qg9ejqvpk6xev-annonce",
    "reference": "TAP_test",
    "title": "test carrer",
    "description": "<p>XXXXXXX</p>",
    "experience": 2,
    "mission": "<p>XXXXX</p>",
    "salary": "0",
    "status": 1,
    "language": "fr_FR",
    "contract_type_id": 1,
    "education_level_id": 3,
    "activity_id": 16,
    "channel_id": 2,
    "metier_id": 44,
    "company_id": 59,
    "mensuality": "y",
    "apply_url": null,
    "currency": "EUR",
    "created_at": "2023-05-31T08:27:15.912Z",
    "updated_at": "2023-07-13T13:29:33.853Z",
    "start_date": null,
    "end_date": null,
    "driver_license": false,
    "remote": "partime",
    "handicap": false,
    "partial": false,
    "meta_title": null,
    "meta_description": null,
    "meta_tags": null,
    "options": {
      "required": ["email", "resume", "motivation"],
      "optionals": ["phone", "social_links"],
      "desactivated": ["indeed"],
      "motivationType": "text"
    },
    "kanban": true,
    "slug_mail": "vpjx6v"
}
```

</details>