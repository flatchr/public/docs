---
sidebar_position: 3
---
# Créer une annonce

:::caution Important

L'URL racine est ici https://api.flatchr.io

:::

## Requête

Il est possible de créer une annonce en utilisant la requête suivante : 

```jsx
POST /company/{companyID}/vacancy
```
### Paramètres
| Paramètre          | In      | Type             | Obligatoire        | Description                                                                               | Exemple                                                                                                                                         |
| ------------------ | ------- | ---------------- | ------------------ | ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| companyId          | path    | string           | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise)           | Wy3EOp2NP3p1KMq8                                                                                                                                |
| title              | payload | string           | <center>✔️</center> | Titre de l'offre                                                                          | Développeur Web                                                                                                                                 |
| reference          | payload | string           |                    | Référence de l'offre                                                                      | dev-web-01                                                                                                                                      |
| description        | payload | string           | <center>✔️</center> | Description de l'offre                                                                    | Nous recherchons un développeur web expérimenté                                                                                                 |
| mission            | payload | string           | <center>✔️</center> | Mission de l'offre                                                                        | Développer des applications web innovantes                                                                                                      |
| profile            | payload | string           | <center>✔️</center> | Profil recherché                                                                          | Expérience en développement web                                                                                                                 |
| experience         | payload | enum             | <center>✔️</center> | Nombre d'années d'expérience requis (voir [experience](/docs/Referentiels/experience.md)) | 1, 2, 4, 6                                                                                                                                      |
| salary             | payload | string           |                    | Salaire proposé                                                                           | 40 000                                                                                                                                          |
| salary_max         | payload | string           |                    | Salaire maximum                                                                           | 60 000                                                                                                                                          |
| contract_type_id   | payload | integer          | <center>✔️</center> | [contract_type_id](/docs/Referentiels/contract_type.md)                                   |                                                                                                                                                 |
| education_level_id | payload | integer          | <center>✔️</center> | [education_level_id](/docs/Referentiels/education_levels.md)                              |                                                                                                                                                 |
| activity_id        | payload | integer          | <center>✔️</center> | [activity_id](/docs/Referentiels/activities.md)                                           |                                                                                                                                                 |
| channel_id         | payload | integer          | <center>✔️</center> | [channel_id](/docs/Referentiels/channels.md)                                              |                                                                                                                                                 |
| metier_id          | payload | integer          | <center>✔️</center> | [metier_id](/docs/Referentiels/metiers.md)                                                |                                                                                                                                                 |
| currency           | payload | string           |                    | Devise du salaire                                                                         | EUR, USD, GBP, CAD, CHF                                                                                                                         |
| language           | payload | string           | <center>✔️</center> | Langue de l'offre                                                                         | fr_FR - en_GB                                                                                                                                   |
| status             | payload | integer          | <center>✔️</center> | Statut de l'offre                                                                         | 1                                                                                                                                               |
| note               | payload | string           |                    | Note supplémentaire                                                                       | Poste à pourvoir rapidement                                                                                                                     |
| mensuality         | payload | enum (y,m,d,h)   |                    | Fréquence de paiement du salaire (year, month, day, hour)                    | *y*,*m*,*d*,*h*                                                                                                                                |
| driver_license     | payload | boolean          |                    | Permis de conduire requis                                                                 | *true* ou *false*                                                                                                                               |
| partial            | payload | boolean          |                    | Poste à temps partiel                                                                     | *true* ou *false*                                                                                                                               |
| remote             | payload | enum             |                    | Poste en télétravail                                                                      | *parttime*, *notime*, *fulltime*                                                                                                                |
| handicap           | payload | boolean          |                    | Poste accessible aux personnes handicapées                                                | *true* ou *false*                                                                                                                               |
| kanban             | payload | boolean          |                    | Utilisation du kanban pour ce recrutement                                                 | *true* ou *false*                                                                                                                               |
| start_date         | payload | date             |                    | Date de début de l'offre                                                                  | 2023-05-01                                                                                                                                      |
| end_date           | payload | date             |                    | Date de fin de l'offre                                                                    | 2024-05-01                                                                                                                                      |
| meta_title         | payload | string           |                    | Titre pour le référencement (SEO)                                                         | Offre d'emploi Développeur Web                                                                                                                  |
| meta_description   | payload | string           |                    | Description pour le référencement (SEO)                                                   | Nous recrutons un développeur web expérimenté pour rejoindre notre équipe                                                                       |
| meta_tags          | payload | string           |                    | Mots-clés pour le référencement (SEO)                                                     | développeur web, recrutement, entreprise                                                                                                        |
| questions          | payload | array            |                    | Tableau d'objet [question](/docs/Schemas/question.md)                                     |                                                                                                                                                 |
| address            | payload | object           | <center>✔️</center> | Objet [address](/docs/Schemas/address.md)                                                 |                                                                                                                                                 |
| tags               | payload | array ou string  |                    | Le(s) id de [tag(s)](/docs/Schemas/tag.md)                                                | ["1234567890123456", "2234567890123456"] ou "1234567890123456"                                                                                  |
| options            | payload | object           |                    | Object [options](/docs/Schemas/options.md)                                                | `{"required": ["email", "resume", "motivation"], "optionals": ["phone", "social_links"], "desactivated": ["indeed"], "motivationType": "text"}` |
| video_url          | payload | string           |                    | Description                                                                               |                                                                                                                                                 |
| show_address       | payload | boolean          |                    | Afficher l'adresse sur l'annonce sur votre site carrière                                  | *true* ou *false*                                                                                                                               |
| show_contract_type | payload | boolean          |                    | Afficher le type de contrat sur l'annonce sur votre site carrière                         | *true* ou *false*                                                                                                                               |
| show_salary        | payload | boolean          |                    | Afficher le salaire sur l'annonce sur votre site carrière                                 | *true* ou *false*                                                                                                                               |
| worker_status      | payload | enum             |                    | Statut du collaborateur                                                                   | *employee*, *engineer*, *executive*, *supervisor*, *technician* , *worker*                                                                      |
| benefits           | payload | array of strings |                    | Avantages liés au poste ou à l'entreprise                                                 | *['Mutuelle', 'Remboursement des transports']*                                                                                                  |
| skills             | payload | strings          |                    | Compétences en lien avec le poste, les valeurs doivent être séparées par des ";"          | *Méticuleux*                                                                                                                                    |
| faqs               | payload | array of object  |                    | Foire aux questions relative à l'annonce                                                  | Voir [faqs schema](/docs/Schemas/faqs)                                                                                                          | ** |

### Exemple de requête
```jsx title="Requête cURL pour Flatchr"
curl -X POST \
  https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/vacancy \
  -H "Authorization: Bearer {token}.KhP5yNcqRlzcBVAHexdVGCnhBPoZ_f5aFJZBtqScnwc" \
  -H 'Content-Type: application/json' \
  -d '{
    "title": "Développeur Web",
    "reference": "dev-web-01",
    "description": "Nous recherchons un développeur web expérimenté",
    "mission": "Développer des applications web",
    "profile": "Expérience en développement web",
    "salary": 40000,
    "salary_max": 60000,
    "contract_type_id": 1,
    "education_level_id": 2,
    "experience": 2,
    "activity_id": 3,
    "channel_id": 4,
    "metier_id": 5,
    "currency": "EUR",
    "language": "fr_FR",
    "status": 1,
    "note": "",
    "mensuality": "y",
    "driver_license": false,
    "partial": false,
    "remote": "fulltime",
    "handicap": false,
    "kanban": false,
    "start_date": "2023-05-01",
    "end_date": "2024-05-01",
    "meta_title": "",
    "meta_description": "",
    "meta_tags": "",
    "questions": [],
    "address": {
        "street_number": "12",
        "route": "rue des Lilas",
        "locality": "Paris",
        "administrative_area_level_2": "Paris",
        "administrative_area_level_1": "Île-de-France",
        "country": "France",
        "postal_code": "75001",
        "formatted_address": "12 rue des Lilas, 75001 Paris, France",
        "location_lat": 48.856614,
        "location_lng": 2.3522219
    },
    "tags": [],
    "options": {},
    "skills": "Méticuleux;Ponctuel",
    "benefits": ["Mutuelle", "Salle de sport"],
    "worker_status": "executive",
    "video_url": "https://youtu.be/6iPzmstn-74",
    "show_address":false,
    "show_contract_type": false,
    "show_contract_date": true,
    "show_salary": false,
    }'
```

## Réponse
| Name               | Type              | Description                                                                  |
| ------------------ | ----------------- | ---------------------------------------------------------------------------- |
| id                 | string            | Identifiant unique de l'offre (encodé)                                       |
| vacancy_id         | string            | Identifiant unique de l'offre (décodé)                                       |
| slug               | string            | Slug de l'offre                                                              |
| reference          | string            | Référence de l'offre d'emploi                                                |
| title              | string            | Titre de l'offre d'emploi                                                    |
| description        | string            | Description de l'offre d'emploi                                              |
| experience         | integer           | Nombre d'années d'expérience requis pour le poste                            |
| mission            | string            | Mission du poste                                                             |
| profile            | string            | Profil recherché pour le poste                                               |
| salary             | integer ou string | Salaire proposé pour le poste                                                |
| status             | integer           | Statut de l'offre d'emploi                                                   |
| language           | string            | Langue de l'offre d'emploi                                                   |
| tags               | array             | Tags associés à l'offre d'emploi                                             |
| contract_type_id   | integer           | Identifiant du type de contrat associé à l'offre d'emploi                    |
| education_level_id | integer           | Identifiant du niveau d'éducation requis pour le poste                       |
| activity_id        | integer           | Identifiant de l'activité associée à l'offre d'emploi                        |
| channel_id         | integer           | Identifiant du canal de diffusion de l'offre d'emploi                        |
| metier_id          | integer           | Identifiant du métier associé à l'offre d'emploi                             |
| company_id         | integer           | Identifiant de l'entreprisequi publie l'offre                                |
| mensuality         | enum (y,m,d,h)    | Fréquence de paiement du salaire (year, month, day, hour)                    |
| apply_url          | string            | URL de la page de candidature pour l'offre d'emploi                          |
| currency           | string            | Devise utilisée pour le salaire                                              |
| created_at         | date              | Date de création de l'offre d'emploi                                         |
| updated_at         | date              | Date de dernière mise à jour de l'offre d'emploi                             |
| start_date         | date              | Date de début du contrat                                                     |
| end_date           | date              | Date de fin du contrat                                                       |
| driver_license     | boolean           | Indication si le permis de conduire est requis pour le poste                 |
| remote             | string            | Indication sur le type de télétravail                                        |
| handicap           | boolean           | Indication si le poste est ouvert aux travailleurs handicapés                |
| partial            | boolean           | Indication si le poste peut être effectué à distance                         |
| kanban             | boolean           | Indication si l'offre d'emploi est intégrée à un tableau Kanban              |
| meta_title         | string            | Titre de la page de référencement (SEO) associée à l'offre d'emploi          |
| meta_description   | string            | La description de la page de référencement (SEO) associée à l'offre d'emploi |
| options            | object            | Options de l'offre d'emploi pour sa visualisation sur les sites carrières    |
| note               | string            | Note associée à l'offre d'emploi (peut être vide)                            |
| address            | object            | Adresse associé à l'offre d'emploi                                           |
| addressFormatted   | string            | Adresse formatée associée à l'offre d'emploi                                 |
| questions          | array             | QUestions associées à l'offre d'emploi                                       |

### Exemple de réponse

<details>
<summary> Voir l'exemple </summary>

```json
{
    "id": "xlV0D9aAaZ9WELkZ",
    "vacancy_id": 1942,
    "slug": "xlv0d9aaaz9welkz-developpeur-web",
    "reference": "dev-web-01",
    "title": "Développeur Web",
    "description": "Nous recherchons un développeur web expérimenté",
    "experience": 3,
    "mission": "Développer des applications web",
    "profile": "Expérience en développement web",
    "salary": "40000",
    "status": 1,
    "language": "fr_FR",
    "tags": [],
    "contract_type_id": 1,
    "education_level_id": 2,
    "activity_id": 3,
    "channel_id": 4,
    "metier_id": 5,
    "company_id": 59,
    "mensuality": "y",
    "apply_url": null,
    "currency": "EUR",
    "created_at": "2023-04-06T08:51:13.584Z",
    "updated_at": "2023-04-06T08:51:13.600Z",
    "start_date": "2023-05-01T00:00:00.000Z",
    "end_date": "2024-05-01T00:00:00.000Z",
    "driver_license": false,
    "remote": "fulltime",
    "handicap": false,
    "partial": false,
    "kanban": false,
    "meta_title": "",
    "meta_description": "",
    "meta_tags": "",
    "options": {},
    "address": {
        "locality": "Paris",
        "location_lat": "48.86",
        "location_lng": "2.35",
        "postal_code": "75001",
        "route": "rue des Lilas",
        "street_number": "12",
        "administrative_area_level_1": "Île-de-France",
        "administrative_area_level_2": "Paris",
        "formatted_address": "12 rue des Lilas, 75001 Paris, France",
        "country": "France"
    },
    "addressFormatted": "12 rue des Lilas, 75001 Paris, France",
    "slug_mail": "vxy98z",
    "questions": [],
    "skills": "Méticuleux;Ponctuel",
    "benefits": ["Mutuelle", "Salle de sport"],
    "worker_status": "executive",
    "video_url": "https://youtu.be/6iPzmstn-74",
    "show_address":false,
    "show_contract_type": false,
    "show_contract_date": true,
    "show_salary": false,
}
```
</details>
