---
sidebar_position: 1
---
# Récupérer l'ensemble de vos annonces

:::caution Important

L'URL racine est ici https://api.flatchr.io

:::

## Requête


```jsx
GET /company/{companyId}/vacancies
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://www.postman.com/flatchr/workspace/flatchr-public-api/request/18861404-e4b0f4b4-e4c7-4315-8e56-058b13afcca9)

### Paramètres
| Paramètre | In    | Type   | Obligatoire        | Description                                                                        | Exemple          |
| --------- | ----- | ------ | ------------------ | ---------------------------------------------------------------------------------- | ---------------- |
| companyId | path  | string | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started.md#identifiant-de-lentreprise) | Wy3EOp2NP3p1KMq8 |
| fields    | query | string |                    | Informations optionnelles                                                          | address          |

### Exemples de requêtes

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
<TabItem value="no_fields" label="Sans fields" default>

```jsx title="Requête cURL"
curl -X GET https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/vacancies
```

</TabItem>

<TabItem value="fields" label="Avec fields">

```jsx title="Requête cURL"
curl -X GET https://api.flatchr.io/company/Wy3EOp2NP3p1KMq8/vacancies?fields=address
```

</TabItem>
</Tabs>

## Réponse
| Name             | Type                                       | Description                                                      |
| ---------------- | ------------------------------------------ | ---------------------------------------------------------------- |
| kanban           | boolean                                    |                                                                  |
| meta_title       |                                            |                                                                  |
| meta_description |                                            |                                                                  |
| meta_tags        |                                            |                                                                  |
| options          | object [options](/docs/Schemas/options.md) | Options de l'offre pour sa visualisation sur les sites carrières |
| contract_type    | string                                     | Type de contrat                                                  |
| education_level  | string                                     | Niveau d'éducation demandé                                       |
| activity         | string                                     | Secteur d'activité                                               |
| channel          | string                                     | Filière                                                          |
| metier           | string                                     | Métier                                                           |
| address          | object [address](/docs/Schemas/address.md) | Ensemble des informations de l'adresse de l'offre                |
| addressFormatted | string                                     | Adresse formatée de l'entreprise                                 |
| company          | object [company](/docs/Schemas/company.md) | Informations de l'entreprise                                     |
| slug_email       | string                                     | Slug de l'email pour l'envoie de candidatures                    |
| questions        | [questions]                                | Tableau de questions                                             |
| activity_id      | integer                                    | Id du secteur d'activité                                         |
| channel_id       | integer                                    | Id de la filière                                                 |
| metier_id        | integer                                    | Id du métier                                                     |
| company_id       | integer                                    | Id de l'entreprise                                               |
| mensuality       | enum (y,m,d,h)                             | Fréquence de paiement du salaire (year, month, day, hour)        |
| apply_url        | string                                     | Url                                                              |
| currency         | enum                                       | Monnaie (ex: EUR)                                                |
| created_at       | timestamp                                  | Date de création de l'annonce                                    |
| updated_at       | timestamp                                  | Date de la mise à jour de l'annonce                              |
| start_date       | timestamp                                  | Date de début du contrat                                         |
| end_date         | timestamp                                  | Date de fin du contrat                                           |
| driver_license   | boolean                                    | Permis de conduire obligatoire                                   |
| remote           | enum                                       | Poste en télétravail ("notime", "parttime", "fulltime")          |
| handicap         | boolean                                    | Postes ouvert aux travailleurs en situation d'handicap           |
| partial          | boolean                                    | Temps partiel                                                    |
| id               | string                                     | Identifiant                                                      |
| vacancy_id       | string                                     | Id de l'annonce                                                  |
| vacancy          | [vacancy](/docs/Schemas/vacancy.md)        |                                                                  |

### Exemple de réponse

<details>
<summary> Voir l'exemple </summary>

```json
[
    {
        "id": "eoLm5NdDXmdAGbZQ",
        "vacancy_id": 1701,
        "slug": "eolm5nddxmdagbzq-archiveur-de-lampes",
        "reference": null,
        "title": "Archiveur de lampes",
        "description": "<p>ihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvihugyicghicg;vn bbjhugyfcfv bnugyffh ug nvaaaa</p>",
        "experience": 2,
        "mission": "<p>Supervise et coordonne l'activité d'un ou plusieurs sites logistiques (plate-forme logistique, dépôt d'unité de production,...) sur les plans : technique (réception, magasinage, stockage, préparation de commandes, expédition, ...), commercial (relation clients, fournisseurs, transporteurs), social (management du personnel) et financier, selon les normes et la réglementation d'hygiène et sécurité et les objectifs qualité (service, coût, délai).<br>Dirige une ou plusieurs équipes (techniciens logistique, chefs d'équipe, opérateurs logistique).</p>",
        "profile": "<p>Superviser la planification de l'exploitation du ou des sites logistiques en fonction de l'activité (flux internes/externes, commandes spécifiques, ...)<br>Superviser l'activité des équipes logistiques<br>Analyser les données d'activité de la structure, du service et identifier des axes d'évolution<br>Analyser les coûts de la chaîne logistique (supply chain)<br>Organiser et coordonner le circuit des informations sur le fonctionnement d'une structure<br>Suivre un budget<br>Concevoir des procédures de gestion<br>Contrôler l'application d'une réglementation<br>Superviser la gestion administrative du personnel<br>Mettre en place des actions de gestion de ressources humaines</p>",
        "salary": "1800",
        "status": 1,
        "language": "fr_FR",
        "contract_type_id": 1,
        "education_level_id": 2,
        "activity_id": 21,
        "channel_id": 8,
        "metier_id": 85,
        "company_id": 59,
        "mensuality": "m",
        "apply_url": null,
        "currency": "EUR",
        "created_at": "2023-05-10T06:40:59.131Z",
        "updated_at": "2023-05-10T06:40:59.145Z",
        "start_date": null,
        "end_date": null,
        "driver_license": false,
        "remote": "notime",
        "handicap": false,
        "partial": false,
        "kanban": true,
        "meta_title": null,
        "meta_description": null,
        "meta_tags": null,
        "options": {
          "required": ["email", "resume", "motivation"],
          "optionals": ["phone", "social_links"],
          "desactivated": ["indeed"],
          "motivationType": "text"
        },
        "slug_mail": "v81n3d",
        "video_url": "",
        "show_address": true,
        "show_contract_date": true,
        "show_contract_type": true,
        "show_salary": true,
        "worker_status": "supervisor",
        "skills": "Merchandising;Négociation",
    },
    {
        "id": "QExy5JpxLEpz7lV1",
        "vacancy_id": 1700,
        "slug": "qexy5jpxlepz7lv1-national-factors-designer",
        "reference": null,
        "title": "National Factors Designer",
        "description": "Senior",
        "experience": 3,
        "mission": "Recusandae quia ex. Iste dolor natus nihil consequatur id. Vel itaque quam quia deleniti harum.",
        "profile": "Quod eos repellat.",
        "salary": null,
        "status": 2,
        "language": "fr_FR",
        "contract_type_id": 1,
        "education_level_id": 2,
        "activity_id": 2,
        "channel_id": 2,
        "metier_id": 2,
        "company_id": 59,
        "mensuality": "y",
        "apply_url": null,
        "currency": "EUR",
        "created_at": "2023-04-28T06:48:19.159Z",
        "updated_at": "2023-04-28T06:48:31.928Z",
        "start_date": null,
        "end_date": null,
        "driver_license": false,
        "remote": "fulltime",
        "handicap": false,
        "partial": false,
        "kanban": true,
        "meta_title": null,
        "meta_description": null,
        "meta_tags": null,
        "options": null,
        "slug_mail": "z4n97d",
        "video_url": "",
        "show_address": true,
        "show_contract_date": false,
        "show_contract_type": false,
        "show_salary": true,
        "worker_status": "supervisor",
        "skills": "Merchandising",
    }
]
```

</details>
