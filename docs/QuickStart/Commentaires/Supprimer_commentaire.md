---
sidebar_position: 3
---


# Supprimer un commentaire

Vous pouvez supprimer un commentaire sur un candidat grâce à l'API.


## Requête

Cette méthode permet de supprimer un commentaire sur un candidat :

```jsx
DELETE /company/{companyID}/applicant/{applicantID}/comment/{commentID}
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)


### Paramètres

| Paramètre   | In   | Type   | Obligatoire        | Description                                                                     | Exemple          |
| ----------- | ---- | ------ | ------------------ | ------------------------------------------------------------------------------- | ---------------- |
| companyID   | path | string | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise) | 0GjqBwprWAdArDl4 |
| applicantID | path | string | <center>✔️</center> | `id` du [candidat](/docs/Schemas/candidate.md)                                  | ADYjo9mGxZdkR0ry |
| commentID   | path | string | <center>✔️</center> | `id` du [commentaire]                                                           | kZ1lVpD9a4o19We7 |


### Exemples de requête

```jsx title="Requête cURL"
curl -X DELETE 'https://api.flatchr.io/company/0GjqNwprWAdArDl4/applicant/ADYjo9mGxZdkRAry/comment/kZ1lVpD9a4o19We7'
    -H "Authorization: Bearer {token}"
    -H 'Content-Type: application/json'
```


## Réponse

| Name       | Type    | Description                  |
| ---------- | ------- | ---------------------------- |
| statusCode | integer | Code statut de la requête    |
| message    | string  | Message expliquant le statut |


### Exemple de réponse

```json
{
    "statusCode": 200,
    "message": "Comment deleted"
}
```
