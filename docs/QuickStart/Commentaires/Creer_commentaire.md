---
sidebar_position: 2
---


# Créer un commentaire

Vous pouvez créer un commentaire sur un candidat grâce à l'API.

:::danger token

Pour créer un commentaire il faut s'authentifier avec un token utilisateur qui peut être récupéré grâce à la méthode [Login](/docs/QuickStart/Login.md)

:::


## Requête

Cette méthode permet de créer un commentaire sur un candidat :

```jsx
POST /company/{companyID}/applicant/{applicantID}/comment
```
[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/18861404-2bd60cea-6942-4809-83e7-e8869748aa62?action=collection%2Ffork&collection-url=entityId%3D18861404-2bd60cea-6942-4809-83e7-e8869748aa62%26entityType%3Dcollection%26workspaceId%3D9ab396af-18af-4f93-809c-cddd2fbd1422)


### Paramètres

| Paramètre   | In    | Type    | Obligatoire        | Description                                                                     | Exemple                                                          |
| ----------- | ----- | ------- | ------------------ | ------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
| companyID   | path  | string  | <center>✔️</center> | [Identifiant de l'entreprise](/docs/getting_started#identifiant-de-lentreprise) | 0GjqBwprWAdArDl4                                                 |
| applicantID | path  | string  | <center>✔️</center> | `id` du [candidat](/docs/Schemas/candidate.md)                                  |
| type        | query | string  | <center>✔️</center> | `type` du [commentaire]                                                         | note                                                             |
| private     | query | boolean |                    | Le commentaire est-il privé ?                                                   | true                                                             |
| file        | query | file    |                    | Pièce jointe                                                                    | zoGaHqRvJ/CV_test.pdf                                            |
| text        | query | string  |                    | Contenu du commentaire                                                          | En réponse au précédent commentaire : très bon niveau en anglais |
| reply_to    | query | string  |                    | `id` du [commentaire] parent                                                    | j4hq6zbqYZ20JA1z                                                 |



### Exemples de requête

```jsx title="Requête cURL"
curl -X POST 'https://api.flatchr.io/company/0GjqBwprWAdArDl4/applicant/ADYjo9mGxZdkN0ry/comment'
    -H "Authorization: Bearer {user_token}"
    -H 'Content-Type: application/json'
    -F 'text="En réponse au précédent commentaire : très bon niveau en anglais"' \
    -F 'file=@"zoGaHrRvJ/CV_test.pdf"' \
    -F 'private="true"' \
    -F 'reply_to="j4hq6zbqYZ20Jn1z"'
```


## Réponse

| Name        | Type                                       | Description                                                                                        |
| ----------- | ------------------------------------------ | -------------------------------------------------------------------------------------------------- |
| id          | integer                                    | Identifiant du commentaire                                                                         |
| text        | string                                     | Le texte du commentaire                                                                            |
| private     | boolean                                    | Le commentaire est-il privé ?                                                                      |
| favorite    | boolean                                    | Le commentaire est-il en favori ?                                                                  |
| created_by  | integer                                    | Identifiant de l'auteur du commentaire                                                             |
| created_at  | timestamp                                  | Date de création du commentaire                                                                    |
| reply_to    | integer                                    | Identifiant du commentaire parent si ce commentaire est une réponse à un commentaire déja existant |
| author      | [author](/docs/Schemas/author.md)          | Auteur du commentaire                                                                              |
| attachments | [attachments](/docs/Schemas/attachment.md) | Les pièces jointes du commentaire                                                                  |
| ratings     | [ratings]                                  |                                                                                                    |


### Exemple de réponse

```json
{
    "id": 3620,
    "text": "En réponse au précédent commentaire : très bon niveau en anglais",
    "private": true,
    "favorite": false,
    "created_by": 391,
    "created_at": "2022-02-15T17:00:00.264Z",
    "reply_to": 3618,
    "author": {
        "id": "lVq5r6pYXk9AmMvP",
        "firstname": "XXXX",
        "lastname": "XXXX"
    },
    "attachments": [],
    "ratings": []
}
```
