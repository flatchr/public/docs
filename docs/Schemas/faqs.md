# faqs

| Name       | Type     | Description                                                                        |
| ---------- | -------- | ---------------------------------------------------------------------------------- |
| id         | integer  | Identifiant de la question                                                         |
| question   | string   | Question                                                                           |
| answer     | string   | Réponse                                                                            |
| company_id | string   | [Identifiant de l'entreprise](/docs/getting_started.md#identifiant-de-lentreprise) |
| created_at | datetime | Date de création                                                                   |
| updated_at | datetime | Date de mise à jour                                                                |