# candidate

|Name|Type|Description|
|---|---|---|
id|integer|Identifiant du candidat en clair|
firstname|string|Prénom /[a-zA-Z]/|
lastname|stroing|Nom /[a-zA-Z]/|
email|string|Email de l'utilisateur|
status|integer|Identifiant du statut du candidat|
external_id|string|Référence externe|
created_at|timestamp|Date de création|
updated_at|timestamp|Date de mise à jour|
summary|||
contact_information|||
comments|||
urls||Lien réseaux sociaux `{facebook: string, linkedin: string, twitter: string, other: string}` éviter de renseigner une string vide|
user_id|||
phone|string|Numéro de téléphone|
consent|boolean|Le candidat a t'il consenti à l'utilisation de ses données|
cv|||
additionals|||