# task

|Name|Type|Description|
|---|---|---|
id|string|Identifiant de la tâche|
type|string|Type (toujours égal à "task")|
description|string|Description de la tâche	|
date|timestamp|Date de la tâche	|
value|[string]|Sur quels éléments porte la tâche : ["applicant=[applicant.id](/docs/Schemas/applicant)"] ou ["member=[user.id](/docs/Schemas/user)"]|
users|[string]|Tableau d'ids des utilisateurs qui peuvent voir la tâche (ex: ["[user.id](/docs/Schemas/user)"])|
memberId|string|`id` de l'[utilisateur](/docs/Schemas/user) qui a créé la tâche|
done|boolean|Est-ce que la tâche a été effectuée ?|
