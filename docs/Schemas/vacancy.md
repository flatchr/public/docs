# vacancy

| Name               | Type                                    | Description                                                            |
| ------------------ | --------------------------------------- | ---------------------------------------------------------------------- |
| id                 | string                                  | Identifiant                                                            |
| vacancy_id         | string                                  | Id de l'annonce                                                        |
| slug               | string                                  | Slug de l'annonce                                                      |
| reference          | string                                  | Référence de l'annonce                                                 |
| title              | string                                  | Titre de l'annonce                                                     |
| description        | string                                  | Description de l'entreprise                                            |
| experience         | integer                                 | Nombre d'années d'expérience                                           |
| mission            | string                                  | Descriptif du poste                                                    |
| profile            | string                                  | Descriptif du profil recherché                                         |
| salary             | integer                                 | Salaire pour le poste                                                  |
| status             | integer                                 | Statut de l'annonce                                                    |
| language           | enum                                    | Langue ex: fr_FR                                                       |
| tags               | [[tag](./tag)]                          | Un tableau de tag                                                      |
| contract_type_id   | integer                                 | Id du [type de contrat](/docs/Referentiels/contract_type.md)           |
| education_level_id | integer                                 | Id du [niveau d'étude](/docs/Referentiels/education_levels.md) demandé |
| activity_id        | integer                                 | Id du [secteur d'activité](/docs/Referentiels/activities.md)           |
| channel_id         | integer                                 | Id de la [filière](/docs/Referentiels/channels.md)                     |
| metier_id          | integer                                 | Id du [métier](/docs/Referentiels/metiers.md)                          |
| company_id         | integer                                 | Id de l'entreprise                                                     |
| mensuality         | enum (y,m,d,h)                          | Fréquence de paiement du salaire (year, month, day, hour)              |
| apply_url          | url                                     |                                                                        |
| currency           | enum                                    | Monnaie ex : "EUR"                                                     |
| created_at         | timestamp                               | Date de création de l'annonce                                          |
| updated_at         | timestamp                               | Date de mise à jour de l'annonce                                       |
| start_date         | timestamp                               | Date de début de contrat                                               |
| end_date           | timestamp                               | Date de fin de contrat                                                 |
| driver_license     | boolean                                 | Permis de conduire obligatoire                                         |
| remote             | enum                                    | Poste en télétravail ("notime", "parttime", "fulltime")                |
| handicap           | boolean                                 | Ouvert aux travailleurs handicapés                                     |
| partial            | boolean                                 | Temps partiel                                                          |
| kanban             | boolean                                 |                                                                        |
| meta_title         |                                         |
| meta_description   |                                         |
| meta_tags          |                                         |
| options            | [options](/docs/Schemas/options.md)     |                                                                        |
| contract_type      | string                                  | Type de contrat                                                        |
| education_level    | string                                  | Niveau d'éducation demandé                                             |
| activity           | string                                  | Secteur d'activité                                                     |
| channel            | string                                  | Filière                                                                |
| metier             | string                                  | Métier                                                                 |
| address            | [address](/docs/Schemas/address.md)     |                                                                        |
| adressFormatted    | string                                  | Adresse de l'entreprise                                                |
| company            | [company](/docs/Schemas/company.md)     |                                                                        |
| slug_mail          | string                                  | slug de l'email pour l'envoi de candidatures                           |
| questions          | [[question](/docs/Schemas/question.md)] | Un tableau de questions                                                |