# options

|Name|Type|Description|
|---|---|---|
required        |[string]  | Les champs qui seront obligatoire lors de la postulation via site carrière
optionals       |[string]  | Les champs qui seront optionnels lors de la postulation via site carrière
desactivated    |[string]  | Les champs qui ne seront pas présents sur le formulaire de postulation sur site carrière
motivationType  |string  | Le type de champs attendu pour la motivation du candidat (`file` ou `text`)

Chacun des tableau `required`, `optionals` et `desactivated` est rempli avec les options que l'on veux sur la page de l'offre sur site carrière. Les différents champs possibles sont:
- email
- phone
- resume
- motivation
- social_links
- indeed
